<?php

namespace app\commands;

use yii\console\Controller;
use Yii;

/**
 * This command handle candidates profile.
 */
class CandidatesController extends Controller
{

    public function actionCreateProfiles()
    {
        echo 'Creating candidate profiles...' . "\n";
        $collection = Yii::$app->mongodb->getCollection('candidates');
        $seniorDelveoperStartSalary = '40000';
        $juniorDelveoperStartSalary = '20000';
        $minSalaryMultiperPerYear = '15000';
        $rows = array();
        for ($i = 0; $i < 5000; $i++) {
            $candidateExperence = rand(1, 10);
            $maxSalary = (($candidateExperence + 1)*$minSalaryMultiperPerYear);
            if($candidateExperence < 3) {
                $minStartSalary = ($candidateExperence*$minSalaryMultiperPerYear) > $juniorDelveoperStartSalary
                        ? $candidateExperence*$minSalaryMultiperPerYear : $juniorDelveoperStartSalary;
                $candidateSalary = rand($minStartSalary, $maxSalary);
                
            } else {
                $minStartSalary = ((
                    $candidateExperence*$minSalaryMultiperPerYear) > $seniorDelveoperStartSalary
                    && $candidateExperence >3
                )
                    ? $candidateExperence*$minSalaryMultiperPerYear
                    : $seniorDelveoperStartSalary;
                $candidateSalary = rand($minStartSalary, $maxSalary);
            }
            echo $candidateExperence. '=>'. $candidateSalary . '=>'. $this->generateRandomString()."\n";
            $rows[] = array('name' => $this->generateRandomString(), 'experience' => $candidateExperence, 'salaryPerMonth' => $candidateSalary);
        }
        $collection->batchInsert($rows);
        
    }
    
    function generateRandomString($length = 10)
    {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
