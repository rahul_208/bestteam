<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\CandidatesSearchForm;
use yii\data\ActiveDataProvider;
use app\models\Candidates;
use yii\filters\VerbFilter;

class CandidatesController extends Controller
{
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Displays candidates.
     *
     * @return string
     */
    public function actionIndex()
    {
        $provider = new ActiveDataProvider([
            'query' => Candidates::find(),
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);
        return $this->render('index', ['dataProvider' => $provider]);
    }
    
     /**
     * Displays search.
     *
     * @return string
     */
    public function actionSearch()
    {
        
        $model = new CandidatesSearchForm();
        $request = \Yii::$app->getRequest();
        if ($request->isAjax) {
            $model->load($request->post());
            $results = $model->findBestTeam();
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $this->renderAjax('searchResult', [
                'results' => $results,
            ]);
        }
        return $this->render('search', [
            'model' => $model,
        ]);
        
    }
    
    public function actionCreate()
    {
        
        $model = new Candidates();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('contactFormSubmitted', 'Record Created successfully');

            return $this->redirect(['view', 'id' => (string)$model->_id]);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
    public function actionUpdate($id)
    {
        
        $model = Candidates::findOne($id);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->update();
            Yii::$app->session->setFlash('contactFormSubmitted', 'Record Created successfully');
            return $this->redirect(['view', 'id' => (string) $model->_id]);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
    public function actionView($id)
    {
        
        $model = Candidates::findOne($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }
    
    public function actionDelete($id)
    {
        Candidates::findOne($id)->delete();
        
        return $this->redirect(['index']);
    }
    
    
}
