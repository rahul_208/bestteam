<?php

namespace app\models;
use yii\mongodb\ActiveRecord;


class Candidates extends ActiveRecord
{
    /**
     * @return string the name of the index associated with this ActiveRecord class.
     */
    public static function collectionName()
    {
        return 'candidates';
    }

    /**
     * @return array list of attribute names.
     */
    public function attributes()
    {
        return ['_id', 'name', 'salaryPerMonth', 'experience'];
    }
    
    
    public function rules()
    {
        return [
            [['name', 'experience', 'salaryPerMonth'], 'required'],
            ['experience', 'in', 'range' => [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]],
            ['salaryPerMonth', 'number'],
            ['salaryPerMonth', 'validateSalary']
        ];
    }
    
    public function validateSalary($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $minJrDeveloperSalary = 20000;
            $minSrDeveloperSalary = 40000;
            $minSalaryForDeveloper = $this->experience >2 ? $minSrDeveloperSalary : $minJrDeveloperSalary;
            $salaryMultiplierPerYear = 15000;
            $minStartSalary = ($this->experience*$salaryMultiplierPerYear) > $minSalaryForDeveloper
                        ? $this->experience*$salaryMultiplierPerYear : $minSalaryForDeveloper;
            if($this->salaryPerMonth < $minStartSalary) {
                $this->addError($attribute, 'minimum start salary should be '. $minStartSalary);
            }
        }
    }
    
}