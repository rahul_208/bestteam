<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\mongodb\Query;

/**
 * CandidatesSearchForm is the model to find best team.
 *
 */
class CandidatesSearchForm extends Model
{
    public $srDeveloper;
    public $jrDeveloper;
    public $budget;
    
    private $candidates = array();
    private $candidatesList = array();
    private $maxCandidateUnit = 0;



    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['srDeveloper', 'jrDeveloper', 'budget'], 'required'],
            ['budget', 'number'],
            [['srDeveloper','jrDeveloper'], 'integer'],
        ];
    }
    
    public function findBestTeam() {
        try {
            $this->prepareData();
            $totalExpense = 0;
            foreach ($this->candidates as $exp) {
                $query = new Query();
                // compose the query
                $query->select(['name', 'salaryPerMonth', 'experience'])
                    ->from('candidates')
                    ->where(['experience' => $exp])
                    ->orderBy(['salaryPerMonth' => 1])
                    ->limit(1);
                // execute the query
                $row = $query->one();
                $this->candidatesList[] = array_merge($row, array('role' => $exp> 2 ? 'Sr Developer' : 'Jr Developer'));
                $totalExpense += $row['salaryPerMonth'];
                //print_r($totalExpense);die;
            }
            if ($totalExpense > $this->budget) {
                if($this->maxCandidateUnit > 1) {
                    $this->maxCandidateUnit--;
                    $this->candidates = array();
                    $this->candidatesList = array();
                    $this->findBestTeam();
                } else {
                    return array('status' => 'error', "errorMsg" => 'Insufficient budget.');
                }

            }
        } catch (\Exception $e) {
            return array('status' => 'error', "errorMsg" => $e->getMessage());
        }
        return array('status' => 'success', "data" => $this->candidatesList);
    }
    
    public function prepareData() {
        $budget = $this->budget;
        $budgetVarience = $this->jrDeveloper * 5000 - $this->srDeveloper *5000;
        $finalBudget  = $budget - $budgetVarience;
        $this->maxCandidateUnit = $this->maxCandidateUnit > 0
                ? $this->maxCandidateUnit
                : (int)($finalBudget/15000) ;// 100000/15000
        $minBudget = ($this->srDeveloper * 40000  + ($this->jrDeveloper* 20000)); //75000
        if($minBudget == 0) {
            throw new \Exception('invalid input.');
        }
        $minUnitRequired = $this->srDeveloper * 3 + $this->jrDeveloper * 1;
        $totalCandidates = $this->srDeveloper + $this->jrDeveloper;
        $differenceUnit = $this->maxCandidateUnit - $minUnitRequired;
        //echo $minUnitRequired;echo $this->maxCandidateUnit;echo $differenceUnit;
        if ($minUnitRequired > $this->maxCandidateUnit) {
            throw new \Exception('Increase your Budget to full fill all requirements.');
        }
        $candidatesExp = array();
        $srDeveloper = $this->srDeveloper;
        $jrDeveloper = $this->jrDeveloper;
        for ($i=0; $i<$totalCandidates; $i++) {
            
            if ($srDeveloper > 0) {
                if($differenceUnit > 7) {
                    $candidatesExp[] = 10;
                    $differenceUnit = $differenceUnit - 7;
                } else {
                    $candidatesExp[] = 3 + $differenceUnit;
                    $differenceUnit = 0;
                }
            } else {
                if ($differenceUnit > 0) {
                    $candidatesExp[] = 2;
                    $differenceUnit--;
                } else {
                    $candidatesExp[] = 1;
                }
            }
            $srDeveloper--;
        
            $this->candidates = $candidatesExp;
        }
        
    }

}
