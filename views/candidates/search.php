<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Search Best Team';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields:</p>

     <?php $form = ActiveForm::begin(['id' => 'search-form']); ?>

        <?= $form->field($model, 'srDeveloper')->textInput(['autofocus' => true]) ?>
        <?= $form->field($model, 'jrDeveloper')->textInput() ?>
        <?= $form->field($model, 'budget')->textInput() ?>
        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary', 'name' => 'search-button']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>
    <div id="search-result">
        
    </div>
<?php $this->registerJs("$('body').on('beforeSubmit', 'form#search-form', function () {
     var form = $(this);
     // return false if form still have some validation errors
     if (form.find('.has-error').length) {
          return false;
     }
     // submit form
     $.ajax({
          url: form.attr('action'),
          type: 'post',
          data: form.serialize(),
          success: function (response) {
            $('#search-result').html(response);
               // do something with response
          }
     });
     return false;
});", \yii\web\View::POS_READY); ?>
</div>
<!--<script>
    
    $('body').on('beforeSubmit', 'form#search-form', function () {
     var form = $(this);
     // return false if form still have some validation errors
     if (form.find('.has-error').length) {
          return false;
     }
     // submit form
     $.ajax({
          url: form.attr('action'),
          type: 'post',
          data: form.serialize(),
          success: function (response) {
               // do something with response
          }
     });
     return false;
});
    </script>-->
