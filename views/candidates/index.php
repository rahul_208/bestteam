<?php
use yii\grid\GridView; 
use yii\helpers\Html;

?>
<?= Html::style('.cand-btn { margin:0 10px 5px 0; } .flt-right {float:right;}') ?>
<?= Html::a('Create Candidate', ['create'], ['class' => 'btn btn-primary flt-right']) ?>
<?= Html::a('Find Best Team', ['search'], ['class' => 'btn btn-primary cand-btn flt-right']) ?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'name',
        'salaryPerMonth',
        'experience',
        ['class' => 'yii\grid\ActionColumn'],
    ],
]) ?>