<?php
if($results['status'] == 'error') {
    echo '<div style="font-size:20px;"><strong>Error:</strong></div>';
    echo '<div style="color:red;"><strong>'. $results['errorMsg'] . '</strong></div>';
} else {
    echo '<div class="clear-fix"></div>';
    echo '<div style="font-size:20px;"><strong>Result:</strong></div>';
    echo '<table class="table-striped" style="width:50%;"><tr><th>Name</th><th>Salary Per Month</th><th>Role</th><th>Experience (Yrs)</th></tr>';
    foreach ($results['data'] as $item) {
        echo '<tr style="width:50%;"><td>'.$item['name'].'</td>';
        echo '<td style="float:right;padding-right:5px;">Rs. '.$item['salaryPerMonth'].'</td>';
        echo '<td>'.$item['role'].'</td>';
        echo '<td>'.$item['experience'].'</td></tr>';

    }
    echo '</table>';
}
